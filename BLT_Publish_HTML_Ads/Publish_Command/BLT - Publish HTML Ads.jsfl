// Copyright © 2017 BLT Communications All Rights Reserved.

/******************************************************************************
 **
 ** @ PUBLISH HTML ADS - version 1.0
 ** @ description: publishes current file to HTML using selected templates
 ** @ developer: Brian Hunkins/Frankie Anaya
 ** @ www.bltcommunications.com
 **
 *****************************************************************************/

/* get dom and run error check */
fl.outputPanel.clear();

var dialog;
var bltProfile;
var thisFileName;
var thisFilePath;
var thisFolderPath;
var messageArray = [];

/** TEMPLATE PATHS **/
var genericTemplatePath     = fl.configURI + 'BLT HTML Templates/BLT_Generic_template.html';
var dcmTemplatePath         = fl.configURI + 'BLT HTML Templates/BLT_DCM_template.html';
var sizmekTemplatePath      = fl.configURI + 'BLT HTML Templates/BLT_Sizmek_template.html';


/** CHECK FOR OPEN DOCUMENT **/
var dom = fl.getDocumentDOM();
dom != undefined ? init() : alert ('THIS COMMAND ONLY FUNCTIONS ON AN OPEN DOCUMENT');


/** INIT **/
function init() {
    thisFileName = dom.name;
    thisFilePath = dom.path;
    thisFolderPath = thisFilePath.substring (0, thisFilePath.lastIndexOf('/') +1);

    // check that templates actually exist in users system
    if (checkTemplateExists (genericTemplatePath) == false
        || checkTemplateExists (dcmTemplatePath) == false
        || checkTemplateExists (sizmekTemplatePath) == false)
    {
        alert ('Template files do not exist or have been renamed. Please copy the template files to: ' + fl.configURI + 'BLT HTML Templates/');
    }

    else {
        // import and store BLT Profile
        dom.importPublishProfile (fl.configURI + 'Publish Profiles/BLT.apr');
        bltProfile = dom.exportPublishProfileString ('BLT');

        // display dialog
        dialog = dom.xmlPanel (fl.configURI + 'Commands/BLT - PublishHTML.xml');
        if (dialog.dismiss != "cancel") {
            if (dialog.dcm == 'true')       setupAd ('dcm');
            if (dialog.sizmek == 'true')    setupAd ('sizmek');
            if (dialog.generic == 'true')   setupAd ('generic');
        }
    }

    // output end results
    fl.outputPanel.clear();
    for (var i = 0; i < messageArray.length; i++) {
        fl.trace (messageArray[i]);
    }

    // remove BLT profile from document
    dom.deletePublishProfile();

    fl.trace ('---------------------------');
    fl.trace ('HTML AD PUBLISHING COMPLETE');
}


// setup and pre-process
function setupAd (templateName) {
    var outputFolder;

    // create folders and process
    switch (templateName) {
        case 'dcm':
            outputFolder = thisFolderPath + 'DCM/';
            processAd (templateName, outputFolder, dcmTemplatePath);
            messageArray.push ('DCM AD PUBLISHED');
            break;

        case 'sizmek':
            outputFolder = thisFolderPath + 'SIZMEK/';
            processAd (templateName, outputFolder, sizmekTemplatePath);
            messageArray.push ('SIZMEK AD PUBLISHED');
            break;

        case 'generic':
            outputFolder = thisFolderPath + 'GENERIC/';
            processAd (templateName, outputFolder, genericTemplatePath);
            messageArray.push ('GENERIC AD PUBLISHED');
            break;
    }
}


// method 2: not make new FLAs and publish direct
function processAd (templateName, outputFolder, templatePath) {
    // create output folder
    FLfile.createFolder ( FLfile.platformPathToURI (outputFolder) );

    // import BLT profile
    dom.importPublishProfile (fl.configURI + 'Publish Profiles/BLT.apr');
    var tempProfile = bltProfile;

    // set export images setting if necessary and apply the profile by importing it
    if (dialog.images == 'true') {
        tempProfile = tempProfile.replace ('<Property name="exportImages">false</Property>', '<Property name="exportImages">true</Property>');
    }

    // modify output path & name
    var fileName = thisFileName.substring (0, thisFileName.length -4);
    var oldFileName = fileName + '.js';
    var newFileName = './' + templateName + '/index.js';
    tempProfile = tempProfile.replace ('<Property name="filename">' + oldFileName + '</Property>', '<Property name="filename">' + newFileName + '</Property>');

    // apply modified profile
    dom.importPublishProfileString (tempProfile);

    // import correct html template & publish
    dom.importCanvasPublishTemplate (templatePath);
    dom.publish();

    // check if zip needed
    if (dialog.zip == 'true') {
        var fileName = thisFileName.substring (0, thisFileName.length -4);
        var sourceFiles = outputFolder + '*';
        var zipFile = fileName + "_" + templateName + ".zip";

        // escape all spaces in path strings
        sourceFiles = sourceFiles.replace (/\s/g, '\\ ');
        zipFile = zipFile.replace (/\s/g, '\\ ');
        outputFolder = outputFolder.replace (/\s/g, '\\ ');

        // cd to directory and zip
        var commandString = 'cd ' + outputFolder + '; zip -r -X -FS ' + zipFile + ' ' + '*';
        FLfile.runCommandLine (commandString);
    }
}


// helper to check if template file exists
function checkTemplateExists (templatePath) {
    return fl.fileExists (templatePath);
}
